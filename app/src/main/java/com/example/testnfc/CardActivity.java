package com.example.testnfc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.vinaygaba.creditcardview.CardType;
import com.vinaygaba.creditcardview.CreditCardView;

import java.util.Objects;

import static com.vinaygaba.creditcardview.CardNumberFormat.MASKED_ALL_BUT_LAST_FOUR;

public class CardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        ActionBar actionBar = getSupportActionBar();

        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        CreditCardView creditCardView= findViewById(R.id.cardView);
        creditCardView.setIsEditable(false);

        creditCardView.setCardNumber(Objects.requireNonNull(intent.getStringExtra("CARD_NUM")));
        creditCardView.setExpiryDate(Objects.requireNonNull(intent.getStringExtra("CARD_EXP_DATE")));
        creditCardView.setCardNumberFormat(MASKED_ALL_BUT_LAST_FOUR);

        int cardType = CardType.AUTO;
        switch (Objects.requireNonNull(intent.getStringExtra("CARD_TYPE"))) {
            case "VISA": {
                cardType = CardType.VISA;
                break;
            }

            case "MASTER_CARD": {
                cardType = CardType.MASTERCARD;
                break;
            }
        }

        creditCardView.setType(cardType);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
