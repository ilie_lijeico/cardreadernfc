package com.example.testnfc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.pro100svitlo.creditCardNfcReader.CardNfcAsyncTask;
import com.pro100svitlo.creditCardNfcReader.utils.CardNfcUtils;

public class MainActivity extends AppCompatActivity implements CardNfcAsyncTask.CardNfcInterface {
    NfcAdapter mNfcAdapter;
    CardNfcUtils mCardNfcUtils;
    boolean mIntentFromCreate;
    CardNfcAsyncTask mCardNfcAsyncTask;

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(getApplicationContext(), "NFC module not found", Toast.LENGTH_LONG).show();
        } else {
            //do something if there are nfc module on device

            mCardNfcUtils = new CardNfcUtils(this);
            //next few lines here needed in case you will scan credit card when app is closed
            mIntentFromCreate = true;
            onNewIntent(getIntent());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIntentFromCreate = false;
        if (mNfcAdapter != null && !mNfcAdapter.isEnabled()) {
            //show some turn on nfc dialog here. take a look in the sample ;-)
            Toast.makeText(getApplicationContext(), "NFC module is not available", Toast.LENGTH_SHORT).show();
        } else if (mNfcAdapter != null){
            mCardNfcUtils.enableDispatch();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mNfcAdapter != null) {
            mCardNfcUtils.disableDispatch();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (mNfcAdapter != null && mNfcAdapter.isEnabled()) {
            //this - interface for callbacks
            //intent = intent :)
            //mIntentFromCreate - boolean flag, for understanding if onNewIntent() was called from onCreate or not
            mCardNfcAsyncTask = new CardNfcAsyncTask.Builder(this, intent, mIntentFromCreate)
                    .build();
        }
    }

    // callbacks
    @Override
    public void startNfcReadCard() {
        //notify user that scanning start
        Toast.makeText(getApplicationContext(), "Scanning in progress...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void cardIsReadyToRead() {
        String card = mCardNfcAsyncTask.getCardNumber();
        String expiredDate = mCardNfcAsyncTask.getCardExpireDate();
        String cardType = mCardNfcAsyncTask.getCardType();

        Intent intent = new Intent(getApplicationContext(), CardActivity.class);

        intent.putExtra("CARD_NUM", card);
        intent.putExtra("CARD_EXP_DATE", expiredDate);
        intent.putExtra("CARD_TYPE", cardType);
        startActivity(intent);
    }

    @Override
    public void doNotMoveCardSoFast() {
        //notify user do not move the card
    }

    @Override
    public void unknownEmvCard() {
        //notify user that current card has unknown nfc tag
    }

    @Override
    public void cardWithLockedNfc() {
        //notify user that current card has locked nfc tag
    }

    @Override
    public void finishNfcReadCard() {
        //notify user that scanning finished
        Toast.makeText(getApplicationContext(), "Scanning finished", Toast.LENGTH_SHORT).show();
    }
}